﻿// QT includes
#include <QApplication>

#include "mainwindow.h"

extern int qInitResources_icons();

int main(int argc, char *argv[])
{
    // QT Stuff
    QApplication app(argc, argv);

    qInitResources_icons();
    
    MainWindow win;
    win.show();
    
    return app.exec();
}
