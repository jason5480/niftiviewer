/*=========================================================================

  Program:   Visualization Toolkit
  Module:    SimpleView.h
  Language:  C++
  Date:      $Date$
  Version:   $Revision$

  Copyright 2009 Sandia Corporation.
  Under the terms of Contract DE-AC04-94AL85000, there is a non-exclusive
  license for use of this work by or on behalf of the
  U.S. Government. Redistribution and use in source and binary forms, with
  or without modification, are permitted provided that this Notice and any
  statement of authorship are reproduced on all copies.

=========================================================================*/
#ifndef _MainWindow_H
#define _MainWindow_H

#include "ui_mainwindow.h"
#include <QMainWindow>
#include <QTimer>
#include "vtkSmartPointer.h"    // Required for smart pointer internal ivars.

// Forward Qt class declarations
class QVTKInteractor;

// Forward VTK class declarations
class vtkRenderer;
class vtkRenderWindow;
class vtkInteractorStyleImage;
class vtkImageSliceMapper;

float getSeconds();

class MainWindow : public QMainWindow, private Ui::MainWindow
{
    Q_OBJECT

public:

    // Constructor/Destructor
    MainWindow();
    ~MainWindow();

public slots:

    virtual void slotOpenFile();
    virtual void slotExit();
    void onPlayPauseClicked();
    void onTimebarMoved(int val);
    void on_wheel_speed_valueChanged(double val);
    void Animate();
    void onQwtSliderMoved(double val);

protected:

protected slots:

private:
    vtkSmartPointer<vtkRenderer> m_renderer;
    vtkSmartPointer<vtkRenderWindow> m_renderWin;
    vtkSmartPointer<QVTKInteractor> m_interactor;
    vtkSmartPointer<vtkInteractorStyleImage> m_interactorStyle;
    vtkSmartPointer<vtkImageSliceMapper> m_imageMapper;
    // Animation
    QTimer m_timer;
    bool m_anim_on;
    float m_anim_last_update;
    float m_anim_speed;
    float m_anim_time;
    float m_anim_end_time;
};

#endif
