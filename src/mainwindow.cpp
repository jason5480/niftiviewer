﻿#include "ui_MainWindow.h"
#include "mainwindow.h"

#include <QFileDialog>
#include <QTimer>
#include <QDatetime>
#include <QDebug>

#include <vtkDataObjectToTable.h>
#include <vtkElevationFilter.h>
#include <vtkPolyDataMapper.h>
#include <vtkNIFTIImageReader.h>
#include <vtkNIFTIImageHeader.h>
#include <vtkImageViewer2.h>
#include <vtkRenderer.h>
#include <vtkRenderWindow.h>
#include <vtkCamera.h>
#include <vtkErrorCode.h>

#include <vtkMatrix4x4.h>
#include "vtkImageData.h"
#include "vtkImageReslice.h"
#include "vtkImageSliceMapper.h"
#include "vtkImageProperty.h"
#include "vtkImageSlice.h"
#include "vtkInteractorStyleImage.h"

#define VTK_CREATE(type, name) \
    vtkSmartPointer<type> name = vtkSmartPointer<type>::New()

#define VTK_INIT(type, name) \
    name = vtkSmartPointer<type>::New()

// Constructor
MainWindow::MainWindow()
{
    setupUi(this);

    m_renderWin = qvtkWidget->GetRenderWindow();
    m_interactor = qvtkWidget->GetInteractor();
    VTK_INIT(vtkInteractorStyleImage, m_interactorStyle);
    m_interactorStyle->SetInteractionModeToImage3D();
    m_interactor->SetRenderWindow(m_renderWin);
    m_interactor->SetInteractorStyle(m_interactorStyle);

    VTK_INIT(vtkRenderer, m_renderer);
    m_renderWin->AddRenderer(m_renderer);

    VTK_INIT(vtkImageSliceMapper, m_imageMapper);

    m_anim_speed = 1;
    wheel_speed->setValue(m_anim_speed);
    // Set up action signals and slots
    connect(actionOpenFile, SIGNAL(triggered()), this, SLOT(slotOpenFile()));
    connect(actionExit, SIGNAL(triggered()), this, SLOT(slotExit()));
    connect(&m_timer, SIGNAL(timeout()), this, SLOT(Animate()));
    connect(btn_play_pause, SIGNAL(clicked()), this, SLOT(onPlayPauseClicked()));
    connect(sliderTimebar, SIGNAL(sliderMoved(int)), this, SLOT(onTimebarMoved(int)));
    connect(qwtSlider, SIGNAL(valueChanged(double)), this, SLOT(onQwtSliderMoved(double)));
};

MainWindow::~MainWindow()
{
    // The smart pointers should clean up for up
}

// Action to be taken upon file open
void MainWindow::slotOpenFile()
{
    VTK_CREATE(vtkNIFTIImageReader, reader);
    //*
    //QString datadir = QCoreApplication::applicationDirPath() + "/../../resources/";
    QString datadir = "D:/MRI/Data_for_visualization/T1_Rest_1/sub01";
    QFileDialog dialog(this);
    QString filename = dialog.getOpenFileName(this, tr("Select .nii file"), datadir, tr("NIFTI Files (*.nii*)"));
    if (!filename.isEmpty()) {
        reader->SetFileName(filename.toStdString().c_str());
        reader->Update();
        reader->PrintHeader(std::cout, vtkIndent(0));
        if (reader->GetTimeAsVector()) cout << "time as vector" << endl;

        if (reader->GetErrorCode() == vtkErrorCode::NoError) {
            reader->Print(std::cout);
            vtkSmartPointer<vtkNIFTIImageHeader> header = reader->GetNIFTIHeader();
            header->PrintSelf(std::cout, vtkIndent(1));
            VTK_CREATE(vtkMatrix4x4, matrix);
            if (reader->GetQFormMatrix()) {
                qDebug() << "Its a QForm";
                matrix->DeepCopy(reader->GetQFormMatrix());
                matrix->Invert();
            }
            else if (reader->GetSFormMatrix()) {
                qDebug() << "Its a SForm";
                matrix->DeepCopy(reader->GetSFormMatrix());
                matrix->Invert();
            }

            VTK_CREATE(vtkImageReslice, reslice);
            reslice->SetInputConnection(reader->GetOutputPort());
            reslice->SetResliceAxes(matrix);
            reslice->SetInterpolationModeToNearestNeighbor();
            reslice->Update();

            double range[2];
            int extent[6];
            reslice->GetOutput()->GetScalarRange(range);
            reslice->GetOutput()->GetExtent(extent);

            qDebug() << "Scalar rage is [" << range[0] << ", " << range[1] << "]";
            qDebug() << "Extent is [" << extent[0] << ", " << extent[1] << "] ["
                                      << extent[2] << ", " << extent[3] << "] [" 
                                      << extent[4] << ", " << extent[5] << "]";

            m_imageMapper->SetInputConnection(reslice->GetOutputPort());
            m_imageMapper->SetOrientation(2);
            m_imageMapper->SliceAtFocalPointOn();

            VTK_CREATE(vtkImageSlice, image);
            image->SetMapper(m_imageMapper);

            image->GetProperty()->SetColorWindow(range[1] - range[0]);
            image->GetProperty()->SetColorLevel(0.5*(range[0] + range[1]));
            image->GetProperty()->SetInterpolationTypeToNearest();

            m_renderer->AddViewProp(image);
            m_renderer->SetBackground(0.0, 0.0, 0.0);
            m_renderer->SetViewport(0.0, 0.0, 1.0, 1.0);

            // use center point to set camera
            double *bounds = m_imageMapper->GetBounds();
            double point[3];
            point[0] = 0.5*(bounds[0] + bounds[1]);
            point[1] = 0.5*(bounds[2] + bounds[3]);
            point[2] = 0.5*(bounds[4] + bounds[5]);
            double maxdim = 0.0;
            for (int j = 0; j < 3; j++) {
                double s = 0.5*(bounds[2 * j + 1] - bounds[2 * j]);
                maxdim = (s > maxdim ? s : maxdim);
            }

            vtkCamera *camera = m_renderer->GetActiveCamera();
            camera->SetFocalPoint(point);
            point[2] -= 500.0;
            camera->SetViewUp(0.0, +1.0, 0.0);

            camera->SetPosition(point);
            camera->ParallelProjectionOn();
            camera->SetParallelScale(maxdim);

            m_renderWin->Render();
        }
    }
    //*/
}

void MainWindow::slotExit() {
    qApp->exit();
}

void MainWindow::onPlayPauseClicked()
{
    qDebug() << "Play/Pause clicked";
    // Pause
    if (m_anim_on)
    {
        btn_play_pause->setText("Play");
        btn_play_pause->setIcon(QIcon(":/Icons/play.png"));
        m_timer.stop();
    }
    // Play
    else
    {
        btn_play_pause->setText("Pause");
        btn_play_pause->setIcon(QIcon(":/Icons/pause.png"));
        m_anim_last_update = getSeconds();
        m_timer.start();
    }

    m_anim_on = !m_anim_on;
}

void MainWindow::Animate()
{
    qDebug() << "Animate";
    // Find current animation time
    double sec = getSeconds();
    m_anim_time += ((sec - m_anim_last_update) * m_anim_speed);
    m_anim_last_update = sec;

    // Stop animation or repeat depending on checkbox value
    if (m_anim_time>m_anim_end_time)
    {
        m_anim_time = 0;

        // Stop
        if (!checkbox_repeat->isChecked())
        {
            m_timer.stop();
            m_anim_on = false;
            sliderTimebar->setValue(sliderTimebar->maximum());
            btn_play_pause->setText("Play");
            btn_play_pause->setIcon(QIcon(":/Icons/play.png"));
            int timebar_position = m_anim_time / m_anim_end_time * sliderTimebar->maximum();
            sliderTimebar->setValue(timebar_position);
            return;
        }
    }

    // Update animatables
    //m_animator_right_eye.Animate(m_anim_time);
    //m_animator_left_eye.Animate(m_anim_time);

    // Update timebar
    int timebar_position = m_anim_time / m_anim_end_time * sliderTimebar->maximum();
    sliderTimebar->setValue(timebar_position);

    // Call update so that changes will be rendered
    qvtkWidget->update();
}

void MainWindow::onTimebarMoved(int val)
{
    qDebug() << "Time Bar Moved " << val;
    if (m_anim_on) return;

    m_anim_time = m_anim_end_time * val / sliderTimebar->maximum();

    // Update animatables
    //m_animator_right_eye.Animate(m_anim_time);
    //m_animator_left_eye.Animate(m_anim_time);

    // Call update so that changes will be rendered
    qvtkWidget->update();
}

void MainWindow::on_wheel_speed_valueChanged(double val)
{
    qDebug() << "Wheel changed value";
    m_anim_speed = wheel_speed->value();
    label_speed_value->setText(QString::number(m_anim_speed, 'f', 2));
}

void MainWindow::onQwtSliderMoved(double val)
{
    qDebug() << "QWT Slider moved " << val;
    m_imageMapper->SetSliceNumber(val/2);
    m_imageMapper->Update();
}

float getSeconds()
{
    static quint64 msec_base = 0;
    quint64 msec = QDateTime::currentMSecsSinceEpoch();
    if (msec_base == 0) msec_base = msec;
    return (float)(msec - msec_base) / 1000.0f;
}
